// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'Arsip Tech Chat',
      meta: [
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'keywords', content: 'Arsip AST, Arsip Tech Chat AST, Arsip Astonishing', },
        { name: 'description', content: 'Website mengenai Arsip Tech Chat server Astonishing', },
        // OpenGraph
        { hid: 'og:title', property: 'og:title', content: 'Arsip Tech Chat' },
        { hid: 'og:url', property: 'og:url', content: 'https://arsiptc.erizazg.my.id', },
        { hid: 'og:description', property: 'og:description', content: 'Website untuk arsip Tech Chat server Discord Astonishing', },
        {
          hid: 'og:image',
          property: 'og:image',
          content:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8F3Q_BrEUhfiDabasjZzYrzqBKQWON6eXdzOP4L0&s',
        },
        // twitter card
        {
          hid: 'twitter:title',
          name: 'twitter:title',
          content: 'Arsip Tech Chat',
        },
        {
          hid: 'twitter:url',
          name: 'twitter:url',
          content: 'https://arsiptc.erizazg.my.id',
        },
        {
          hid: 'twitter:description',
          name: 'twitter:description',
          content: 'Website untuk arsip Tech Chat server Discord Astonishing',
        },
        {
          hid: 'twitter:image',
          name: 'twitter:image',
          content:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8F3Q_BrEUhfiDabasjZzYrzqBKQWON6eXdzOP4L0&s',
        },
        {
          hid: 'twitter:domain',
          name: 'twitter:domain',
          content: 'arsiptc.erizazg.my.id',
        },
      ],
      script: [
        { src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js' },
        { src: 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js' },
        { src: 'https://cdnjs.cloudflare.com/ajax/libs/lottie-web/5.8.1/lottie.min.js' },
        { src: '@/public/script.js', },
      ],
      link: [
        { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/solarized-dark.min.css'},
        { rel: 'canonical', href: 'https://arsiptc.erizazg.my.id'},
        { rel: 'icon', type: 'image/png', href: 'https://raw.githubusercontent.com/Eriza-Z/tech-chat/master/public/Icon.png' }
      ],
      noscript: [{ children: 'Javascript is required' }],
    },
  },
  css: ['~/public/style.css'],
});
